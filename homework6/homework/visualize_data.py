from .utils import SuperTuxDataset

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

from . import dense_transforms


def main(args):
    transform=dense_transforms.Compose([dense_transforms.ColorJitter(brightness=0.05, contrast=0.05, saturation=0.05, hue=0.25),dense_transforms.ToTensor()])
    dataset = SuperTuxDataset(args.dataset, transform=transform)

    idxes = np.random.randint(len(dataset), size=args.N)
    f, axes = plt.subplots(1, args.N, figsize=(4 * args.N, 4))

    for i, idx in enumerate(idxes):
        img, point = dataset[idx]
        axes[i].imshow(img.permute(1, 2, 0))
        axes[i].axis('off')
        circle = Circle(point, ec='r', fill=False, lw=2)
        axes[i].add_patch(circle)

    plt.show()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('dataset')
    parser.add_argument('-N', type=int, default=5)

    main(parser.parse_args())
