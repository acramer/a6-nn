import numpy as np
import pystk
from random import random


def control(aim_point, current_vel):
    """
    Set the Action for the low-level controller
    :param aim_point: Aim point, in local coordinate frame
    :param current_vel: Current velocity of the kart
    :return: a pystk.Action (set acceleration, brake, steer, drift)
    """
    action = pystk.Action()

    """
    Your code here
    Hint: Use action.acceleration (0..1) to change the velocity. Try targeting a target_velocity (e.g. 20).
    Hint: Use action.brake to True/False to brake (optionally)
    Hint: Use action.steer to turn the kart towards the aim_point, clip the steer angle to -1..1
    Hint: You may want to use action.drift=True for wide turns (it will turn faster)
    """
    aim_point[0] = np.arctan2([aim_point[0]], [aim_point[2]])[0]*(180/np.pi)
    target_vel = 30
    action.brake = False
    action.acceleration = 0
    action.steer = (0.25)*aim_point[0]
    action.drift = False
    action.nitro = False

    control.deltas.pop(0)
    control.deltas.append((aim_point[0]-control.aim_point[0], aim_point[1]-control.aim_point[1], aim_point[2]-control.aim_point[2]))
    control.deltas2.pop(0)
    control.deltas2.append(abs(control.deltas[-1][0]-control.deltas[-2][0]))
    control.aim_point = aim_point
    if all([i>30 for i in control.deltas2]):
        action.steer = 0
    elif control.deltas2[-1] > 45:
        action.steer = control.last_steer
    control.last_steer = action.steer

    # Track states
    # aim_point[0] = aim_point[0] + (random()*2-1)*4
    # if abs(aim_point[0])>50 and aim_point[2]<2:
    #     action.acceleration = 1
    #     return action
    if abs(aim_point[0])>33 and aim_point[2]<12:
        if current_vel < 5:
            action.acceleration = 0.2
        else:
            action.brake = True
        # print("AP: ({: =8.3f}, {: =10.3f}, {: =8.1f}) | ({: =8.3f}, {: =10.3f}, {: =8.1f}) | {: =8.1f} | {:5}".format(aim_point[0], aim_point[1], aim_point[2], control.deltas[-1][0], control.deltas[-1][1], control.deltas[-1][2], abs(control.deltas[-1][0]-control.deltas[-2][0]), action.brake))
        return action
    elif abs(aim_point[0])>23:
        action.drift = True
    # print("AP: ({: =8.3f}, {: =10.3f}, {: =8.1f}) | ({: =8.3f}, {: =10.3f}, {: =8.1f}) | {: =8.1f}".format(aim_point[0], aim_point[1], aim_point[2], control.deltas[-1][0], control.deltas[-1][1], control.deltas[-1][2], abs(control.deltas[-1][0]-control.deltas[-2][0])))
    if current_vel > target_vel:
        action.brake = True
        return action
    action.acceleration = 1 - current_vel*(7/10)/target_vel + aim_point[2]/70
    if current_vel < 1.5 or (current_vel < 12 and abs(aim_point[0]) <= 7.5 and aim_point[2] > 14):
        action.acceleration = 1
        action.nitro = True
        action.brake = False

    return action
control.aim_point = (0,0,0)
control.deltas = [(0,0,0)]*5
control.deltas2 = [0]*5
control.last_steer = 0


if __name__ == '__main__':
    from .utils import PyTux
    from argparse import ArgumentParser


    def test_controller(args):
        import numpy as np
        pytux = PyTux()
        for t in args.track:
            steps = pytux.rollout(t, control, max_frames=1000, verbose=args.verbose)
            print(steps)
        pytux.close()


    parser = ArgumentParser()
    parser.add_argument('track', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()
    test_controller(args)
